<?php

// Se inicia sesión
session_start();

// Se incluye archivo de configuración
require_once('Config.php');
function autoLoadClass($class)
{
	require_once('class/' . $class . '.php');
}

spl_autoload_register('autoLoadClass');
