<?php
class User
{
	// Inicializando variable
	private $db;
	private $table;
	/*
	* Constructor de la clase
	*/

	public function __construct()
	{
		$this->db = new Database;
		$this->table = "users";
	}

	public function agregarRegistro($data)
	{
		if ($this->db->insert("users", $data)) {
			$last_id = $this->db->lastInsertId();
			$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id;");
			$this->db->bind(':id', $last_id, PDO::PARAM_INT);
			$result = $this->db->single();
			return $result;
		} else {
			return 0;
		}
	}

	public function usuarios()
	{
		$this->db->query("SELECT * FROM " . $this->table . ";");
		$resultados = $this->db->resultset();
		return $resultados;
	}

	public function actualizarRegistro($data, $id)
	{
		if ($this->db->updates($this->table, $data, $id)) {
			return true;
		} else {
			return false;
		}
	}

	public function eliminarRegistro($id)
	{
		if ($this->db->deletes($this->table, $id)) {
			return true;
		} else {
			return false;
		}
	}
}
