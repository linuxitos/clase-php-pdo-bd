DROP DATABASE IF EXISTS database_test;
CREATE DATABASE IF NOT EXISTS database_test DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE database_test;

--
-- Table structure for table users
--

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  gender varchar(255) NOT NULL,
  hobbie varchar(255) NOT NULL,
  country varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table users
--

INSERT INTO users (name, last_name, email, gender, hobbie, country) VALUES
('Angela', 'Carrillo', 'Carrillo@gmail.com', 'female', 'Movies,Reading,Magazine,', 'Usa'),
('Eric', 'Olney', 'Olney@gmail.com', 'male', 'Cricket,TV,', 'Usa'),
('Charles', 'Moreno', 'Moreno@gmail.com', 'male', 'Cricket,Reading,Magazine,', 'Usa');
